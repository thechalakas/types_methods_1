﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_Methods_1
{
    //this is the default class for the Main method
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    //Defining a new type 
    //this type will help me do basic functionality of a calculator.

    class Calculator
    {
        //this method adds the behavior of adding two numbers to the class Calculator
        //notice how it takes two parameters - a and b
        //notice the 'int' after the 'public' keyword
        //this means, the method will return an int value
        public int add_two_numbers(int a, int b)
        {
            //I have created a object of type int
            //I have given it meaningful name - just like I have for everything else in this program
            int result_addition = a + b;  //adding the two parameters
            return (result_addition);  //returning the result
        }

        //this is just like above but here, I am not returning any value after doing the calculation
        //so I am putting the return type of this method as void
        public void add_two_numbers_2(int a,int b)
        {
            //I have created a object of type int
            //I have given it meaningful name - just like I have for everything else in this program
            int result_addition = a + b;  //adding the two parameters

            //at this point the addition is completed. I can show output using debug or write the output to console
            //I can also write the output to a file or a server
            //anything can be done but I am for now, not writing the output anywhere

        }

        //this is similar to above except it does not return a value, nor does it accept any parameters
        public void add_two_numbers_3()
        {
            //instantiating and assigning two types
            int a = 10;
            int b = 20;
            int result_of_addition = a + b;
        }

        //one more thing
        //now, for the parameters, I have used 'a' and 'b'. although they make sense in the context above
        //it is not a good habit to use such arbitratry names
        //perhaps using names such as 'number_1' and 'number_2' would better

    }
}
